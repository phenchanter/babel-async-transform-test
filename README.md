# README #

## How to

    npm install
    npm start
    npm test

By default Babel transforms this

    async function foo() {
        await bar();
    }

into this

    var _asyncToGenerator = function (fn) {
      ...
    };
    var foo = _asyncToGenerator(function* () {
      yield bar();
    });

And it means that we must carefully check where to define function. i.e it is impossible to define function somewhere in the bottom of module and use it at the top

I suppose that async functions should behave like default functions i.e. bubble up definition of function.

And traceur compiler works exactly like this

    npm run-script test-traceur
