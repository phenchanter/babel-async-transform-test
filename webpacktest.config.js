var config = {
    entry: {
        babel:'./test/index.babel.test.js',
        traceur:'./test/index.traceur.test.js'
    },
    output: {
        path:'./build/',
        filename:'[name].bundle.js'
    },
    module: {
        loaders: [
            {
                test: /.*babel.*js$/,
                loader: 'babel-loader',
                exclude: /(node_modules)/,
                query: {
                    plugins:['transform-regenerator'],
                    presets: ['es2015', 'stage-0']
                }
            },
            {
                test: /.*traceur.*js$/,
                loader: 'traceur-loader?runtime&experimental&asyncFunctions',
                exclude: /(node_modules)/

            }
        ],
    },
};

module.exports = config;