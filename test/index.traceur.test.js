let Module = require('../index.traceur.js');
let expect = require('expect');


// Suite
describe('Module', function () {

    // Test
    it('is available', function () {
        expect(Module).toExist();
    });

    it('has getter for event name', function () {
        expect(Module.getter()).toEqual(['function','function']);
    });
});